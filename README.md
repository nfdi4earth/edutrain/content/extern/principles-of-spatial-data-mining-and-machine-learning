## Resources
Adopted from: [Repository](https://github.com/yuzzfeng/Coding4Geo)
## Administration
Farzaneh Sadeghi

![Creative Commons Attribution License](https://licensebuttons.net/l/by/4.0/88x31.png)  
This work is licensed under a [Creative Commons Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/).
